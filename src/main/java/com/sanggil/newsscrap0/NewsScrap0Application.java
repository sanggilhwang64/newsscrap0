package com.sanggil.newsscrap0;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NewsScrap0Application {

    public static void main(String[] args) {
        SpringApplication.run(NewsScrap0Application.class, args);
    }

}
